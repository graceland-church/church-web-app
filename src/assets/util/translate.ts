import { from, Observable } from "rxjs";
import { TranslateLoader } from "@ngx-translate/core";

export class LazyTranslateLoader implements TranslateLoader {
  constructor(private path: string) {}
  getTranslation(lang: string): Observable<any> {
    return from(import(`../util/i18n/${this.path}/${lang}.json`));
  }
}
