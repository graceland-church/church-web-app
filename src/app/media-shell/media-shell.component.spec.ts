import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaShellComponent } from './media-shell.component';

describe('MediaShellComponent', () => {
  let component: MediaShellComponent;
  let fixture: ComponentFixture<MediaShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
