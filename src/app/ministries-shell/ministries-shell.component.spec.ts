import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinistriesShellComponent } from './ministries-shell.component';

describe('MinistriesShellComponent', () => {
  let component: MinistriesShellComponent;
  let fixture: ComponentFixture<MinistriesShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinistriesShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinistriesShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
