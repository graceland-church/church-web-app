import { on, createReducer, Action } from "@ngrx/store";

export const GRACELANDCHURCH_FEATURE_KEY = "gracelandchurch";

export interface GracelandChurchState {
  sermons: any[];
  media: any[];
  loadingState: boolean;
  submitting: boolean;
}
export interface GracelandChurchPartialState {
  readonly [GRACELANDCHURCH_FEATURE_KEY]: GracelandChurchState;
}

export const initialState: GracelandChurchState = {
  sermons: [],
  media: [],
  loadingState: false,
  submitting: false,
};

export const reconsiderationReducer = createReducer(initialState);
export function reducer(state: GracelandChurchState, action: Action) {
  return reconsiderationReducer(state, action);
}
