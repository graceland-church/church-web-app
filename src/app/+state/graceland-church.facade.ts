import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { GracelandChurchPartialState } from "./graceland-church.reducer";

@Injectable({
  providedIn: "root",
})
export class GracelandChurchFacade {
  constructor(private store$: Store<GracelandChurchPartialState>) {}
}
