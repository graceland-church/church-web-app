import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyShellComponent } from './body-shell.component';

describe('BodyShellComponent', () => {
  let component: BodyShellComponent;
  let fixture: ComponentFixture<BodyShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
