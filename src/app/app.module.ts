import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogModule } from '@angular/material/dialog';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { NouisliderModule } from 'ng2-nouislider';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModernHeroComponent } from './modern-hero/modern-hero.component';
import { MultipurposeBannerComponent } from './multipurpose-banner/multipurpose-banner.component';
import { SermonsShellComponent } from './sermons-shell/sermons-shell.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { MediaShellComponent } from './media-shell/media-shell.component';
import { HomeShellComponent } from './home-shell/home-shell.component';
import { AboutUsShellComponent } from './about-us-shell/about-us-shell.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './+state/graceland-church.reducer';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { GracelandChurchEffects } from './+state/graceland-church.effects';
import { LazyTranslateLoader } from 'src/assets/util/translate';
import { CommonModule } from '@angular/common';
import { SharedModule } from './shared/shared.module';
import { HeaderShellComponent } from './header-shell/header-shell.component';
import { FooterShellComponent } from './footer-shell/footer-shell.component';
import { BodyShellComponent } from './body-shell/body-shell.component';
import { EventsShellComponent } from './events-shell/events-shell.component';
import { WelcomeShellComponent } from './welcome-shell/welcome-shell.component';
import { MinistriesShellComponent } from './ministries-shell/ministries-shell.component';
import { ConnectDialogComponent } from './connect-dialog/connect-dialog.component';

@NgModule({
	declarations: [
		AppComponent,
		ModernHeroComponent,
		MultipurposeBannerComponent,
		SermonsShellComponent,
		AboutUsComponent,
		MediaShellComponent,
		HomeShellComponent,
		AboutUsShellComponent,
		HeaderShellComponent,
		FooterShellComponent,
		BodyShellComponent,
		EventsShellComponent,
		WelcomeShellComponent,
		MinistriesShellComponent,
		ConnectDialogComponent
	],
	imports: [
		CommonModule,
		BrowserModule,
		MatDialogModule,
		FormsModule,
		JwBootstrapSwitchNg2Module,
		NouisliderModule,
		HttpClientModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		NgbModule,
		SharedModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: () => new LazyTranslateLoader('gracelandchurch')
			},
			isolate: true
		}),
		StoreModule.forRoot(reducer),
		EffectsModule.forRoot([ GracelandChurchEffects ]),
		StoreRouterConnectingModule.forRoot(),
		!environment.production ? StoreDevtoolsModule.instrument() : []
	],
	exports: [ TranslateModule ],
	providers: [],
	entryComponents: [ ConnectDialogComponent ],
	bootstrap: [ AppComponent ]
})
export class AppModule {
	constructor(translate: TranslateService) {
		// set default lang and load copy bundle
		translate.setDefaultLang('en');
		translate.use('en');
	}
}
