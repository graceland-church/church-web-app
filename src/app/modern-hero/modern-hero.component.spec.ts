import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModernHeroComponent } from './modern-hero.component';

describe('ModernHeroComponent', () => {
  let component: ModernHeroComponent;
  let fixture: ComponentFixture<ModernHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModernHeroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModernHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
