import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-connect-dialog',
	templateUrl: './connect-dialog.component.html',
	styleUrls: [ './connect-dialog.component.scss' ]
})
export class ConnectDialogComponent {
	constructor(public activeModal: NgbActiveModal) {}
}
