import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderShellComponent } from './header-shell.component';

describe('HeaderShellComponent', () => {
  let component: HeaderShellComponent;
  let fixture: ComponentFixture<HeaderShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
