import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterShellComponent } from './footer-shell.component';

describe('FooterShellComponent', () => {
  let component: FooterShellComponent;
  let fixture: ComponentFixture<FooterShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
