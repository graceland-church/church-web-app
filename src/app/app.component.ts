import {
  Component,
  OnInit,
  Inject,
  Renderer2,
  ElementRef,
  ViewChild,
} from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { DOCUMENT } from "@angular/common";
import { Location } from "@angular/common";
import { HeaderShellComponent } from "./header-shell/header-shell.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  private _router: Subscription;
  @ViewChild(HeaderShellComponent, { static: true })
  navbar: HeaderShellComponent;

  constructor(
    private renderer: Renderer2,
    private router: Router,
    @Inject(DOCUMENT) private document: any,
    private element: ElementRef,
    public location: Location
  ) {}
  ngOnInit() {
    var navbar: HTMLElement = this.element.nativeElement.children[0]
      .children[0];
    this._router = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (window.outerWidth > 991) {
          window.document.children[0].scrollTop = 0;
        } else {
          window.document.activeElement.scrollTop = 0;
        }
        this.navbar.sidebarClose();
      });
    this.renderer.listen("window", "scroll", (event) => {
      const number = window.scrollY;
      if (number > 150 || window.pageYOffset > 150) {
        // add logic
        navbar.classList.remove("navbar-transparent");
        document.getElementById("youtube").classList.add("youtube-color");
        document.getElementById("facebook").classList.add("facebook-color");
        document.getElementById("instagram").classList.add("instagram-color");
      } else {
        // remove logic
        navbar.classList.add("navbar-transparent");
        document.getElementById("youtube").classList.remove("youtube-color");
        document.getElementById("facebook").classList.remove("facebook-color");
        document
          .getElementById("instagram")
          .classList.remove("instagram-color");
      }
    });
    var ua = window.navigator.userAgent;
    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      var version = parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }
  }
}
