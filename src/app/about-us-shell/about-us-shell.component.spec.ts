import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutUsShellComponent } from './about-us-shell.component';

describe('AboutUsShellComponent', () => {
  let component: AboutUsShellComponent;
  let fixture: ComponentFixture<AboutUsShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutUsShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutUsShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
