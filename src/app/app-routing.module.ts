import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AboutUsShellComponent } from "./about-us-shell/about-us-shell.component";
import { HomeShellComponent } from "./home-shell/home-shell.component";
import { MediaShellComponent } from "./media-shell/media-shell.component";
import { TranslateModule } from "@ngx-translate/core";
import { EventsShellComponent } from "./events-shell/events-shell.component";
import { WelcomeShellComponent } from "./welcome-shell/welcome-shell.component";
import { AppComponent } from "./app.component";
import { MinistriesShellComponent } from "./ministries-shell/ministries-shell.component";
import { SermonsShellComponent } from "./sermons-shell/sermons-shell.component";

const routes: Routes = [
  {
    path: "home",
    component: HomeShellComponent,
  },
  {
    path: "welcome",
    component: WelcomeShellComponent,
  },
  {
    path: "about",
    component: AboutUsShellComponent,
  },
  {
    path: "media",
    component: MediaShellComponent,
    children: [
      {
        path: "sermons",
        component: SermonsShellComponent,
      },
    ],
  },
  {
    path: "ministries",
    component: MinistriesShellComponent,
  },
  {
    path: "events",
    component: EventsShellComponent,
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
    }),
  ],
  exports: [RouterModule, TranslateModule],
})
export class AppRoutingModule {}
