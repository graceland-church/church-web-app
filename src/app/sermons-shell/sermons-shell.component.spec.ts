import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SermonsShellComponent } from './sermons-shell.component';

describe('SermonsShellComponent', () => {
  let component: SermonsShellComponent;
  let fixture: ComponentFixture<SermonsShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SermonsShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SermonsShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
