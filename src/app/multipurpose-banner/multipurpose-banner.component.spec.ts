import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipurposeBannerComponent } from './multipurpose-banner.component';

describe('MultipurposeBannerComponent', () => {
  let component: MultipurposeBannerComponent;
  let fixture: ComponentFixture<MultipurposeBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipurposeBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipurposeBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
