import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConnectDialogComponent } from '../connect-dialog/connect-dialog.component';

@Component({
	selector: 'app-home-shell',
	templateUrl: './home-shell.component.html',
	styleUrls: [ './home-shell.component.scss' ]
})
export class HomeShellComponent {
	focus1: boolean;
	focus2: boolean;
	constructor(private modalService: NgbModal) {}

	openConnect() {
		const modalRef = this.modalService.open(ConnectDialogComponent);
	}
}
