import { CommonModule } from "@angular/common";
import { NgModule, PipeTransform } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

export declare class TranslatePipe implements PipeTransform {
  /**
   * @param value a string to be internationalized.
   * @param mapping an object that indicates the text that should be displayed
   * for different values of the provided `value`.
   */
  transform(
    value: string | null | undefined,
    mapping: {
      [key: string]: string;
    }
  ): string;
}

@NgModule({
  imports: [CommonModule, FormsModule, RouterModule, TranslateModule],
  declarations: [],
  exports: [CommonModule, FormsModule, RouterModule],
})
export class SharedModule {}
